package com.softserve.edu.alina_shulghina.numbers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alin- on 16.12.2017.
 */
public class Digits {

    private static Map<Integer, String> digits = new HashMap<>();

    static {
        digits.put(0, "");
        digits.put(1, "один");
        digits.put(2, "два");
        digits.put(3, "три");
        digits.put(4, "четыре");
        digits.put(5, "пять");
        digits.put(6, "шесть");
        digits.put(7, "семь");
        digits.put(8, "восемь");
        digits.put(9, "девять");
    }

    public static String getWordForNumber(int n) {
        return digits.get(n);
    }

    //if after one goes word 'thousand' return proper female form of 'one'
    public static String getOneProperForm(boolean isThousandAfter) {
        if (isThousandAfter) return "одна";
        else return digits.get(1);
    }

    //if after two goes word 'thousand' return proper female form of 'two'
    public static String getTwoProperForm(boolean isThousandAfter) {
        if (isThousandAfter) return "две";
        else return digits.get(2);
    }
}
