package com.softserve.edu.alina_shulghina.numbers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alin- on 16.12.2017.
 */
public class Tens {

    private static Map<Integer, String> tens = new HashMap<>();

    static {
        tens.put(1, "десять");
        tens.put(2, "двадцать");
        tens.put(3, "тридцать");
        tens.put(4, "сорок");
        tens.put(5, "пятьдесят");
        tens.put(6, "шестьдесят");
        tens.put(7, "семьдесят");
        tens.put(8, "восемьдесят");
        tens.put(9, "девяносто");
    }


    public String getWordForNumber(int n) {
        return tens.get(n);
    }
}
