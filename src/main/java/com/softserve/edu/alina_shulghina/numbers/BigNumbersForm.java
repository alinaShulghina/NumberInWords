package com.softserve.edu.alina_shulghina.numbers;

import java.util.HashMap;
import java.util.Map;

/*
    Class for storing and giving proper form of big numbers.
    Big number is a number more than thousand (million, billion etc)
    This numbers have the same endings.
 */
public class BigNumbersForm {
    private static Map<Integer, String> endings;

    static {
        endings = new HashMap<>();
        endings.put(1, "");
        endings.put(2, "а");
        endings.put(3, "а");
        endings.put(4, "а");
    }

    public static String getNumberProperForm(int n, BigNumbers numberName) {
        int remainder = n % 10;
        if (!endings.containsKey(remainder)
                || (n > 10 && endings.containsKey(remainder))) return numberName.getName() + "ов";
        return numberName.getName() + endings.get(remainder);
    }

    /*
        Enum for big numbers' strings. Each value is associated with its russian string
     */
    public enum BigNumbers {
        MILLION("миллион", 1_000_000), BILLION("миллиард", 1_000_000_000), TRILLION("триллион", 1000000000000L);

        private String name;

        private long number;

        BigNumbers(String numberName, long number) {
            this.name = numberName;
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public static BigNumbers getNumber(int number) {
            for (BigNumbers bigNumber : values()) {
                if (number == bigNumber.number) {
                    return bigNumber;
                }
            }
            throw new IllegalArgumentException("No numbers found");
        }
    }
}
