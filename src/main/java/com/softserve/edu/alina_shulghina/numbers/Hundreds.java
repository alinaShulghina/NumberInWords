package com.softserve.edu.alina_shulghina.numbers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alin- on 16.12.2017.
 */
public class Hundreds {

    private static Map<Integer, String> hundreds = new HashMap<>();

    static {
        hundreds.put(1, "сто");
        hundreds.put(2, "двести");
        hundreds.put(3, "триста");
        hundreds.put(4, "четыреста");
        hundreds.put(5, "пятьсот");
        hundreds.put(6, "шестьсот");
        hundreds.put(7, "семьсот");
        hundreds.put(8, "восемьсот");
        hundreds.put(9, "девятьсот");
    }

    public static String getWordForNumber(int n) {
        return hundreds.get(n);
    }
}
