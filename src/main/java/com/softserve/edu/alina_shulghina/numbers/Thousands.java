package com.softserve.edu.alina_shulghina.numbers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alin- on 16.12.2017.
 */
public class Thousands {

    private static Map<Integer, String> numbers = new HashMap<>();

    static {
        numbers.put(1, "тысяча");
        numbers.put(2, "тысячи");
        numbers.put(3, "тысячи");
        numbers.put(4, "тысячи");
    }

    public static String getThousandProperForm(int n) {
        int remainderFrom10 = n % 10;
        int remainderFrom100 = n % 100;
        if (!numbers.containsKey(remainderFrom10)
                || (remainderFrom100 == 11
                    || remainderFrom100 == 12
                    || remainderFrom100 == 13
                    || remainderFrom100 == 14)) return "тысяч";
        return numbers.get(remainderFrom10);
    }
}
