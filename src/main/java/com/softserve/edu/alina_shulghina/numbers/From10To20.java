package com.softserve.edu.alina_shulghina.numbers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alin- on 16.12.2017.
 */
public class From10To20 {

    private static Map<Integer,String> numbers = new HashMap<>();

    static  {
        numbers.put(10,"десять");
        numbers.put(11,"одиннадцать");
        numbers.put(12,"двенадцать");
        numbers.put(13,"тринадцать");
        numbers.put(14,"четырнадцать");
        numbers.put(15,"пятнадцать");
        numbers.put(16,"шестнадцать");
        numbers.put(17,"семнадцать");
        numbers.put(18,"восемнадцать");
        numbers.put(19,"девятнадцать");
    }


    public static String getWordForNumber(int n) {
        return numbers.get(n);
    }
}
