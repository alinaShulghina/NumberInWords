package com.softserve.edu.alina_shulghina;

import com.softserve.edu.alina_shulghina.numbers.*;

import com.softserve.edu.alina_shulghina.numbers.BigNumbersForm.BigNumbers;

/**
 * Created by alin- on 16.12.2017.
 */
public class NumberParser {

    public static String numberInWords = "";
    public static Boolean femaleNumber = false; // use for proper form of digit before thousand( which is female)

    public static String convertNumber(long number) {
        String thousandForm;
        String bigNumberForm;
        int countDigitsInNumber = (int) Math.log10(number) + 1;
        if (countDigitsInNumber == 1) {
            if (number == 1) {
                String oneString = Digits.getOneProperForm(femaleNumber);
                femaleNumber = false;
                return oneString;
            }
            if (number == 2) {
                String twoString = Digits.getTwoProperForm(femaleNumber);
                femaleNumber = false;
                return twoString;
            }
            femaleNumber = false;
            return Digits.getWordForNumber((int) number);
        }
        if (countDigitsInNumber == 2) {
            femaleNumber = false;
            if (number < 20) return From10To20.getWordForNumber((int) number);
            return new Tens().getWordForNumber((int) (number / 10)) + ((number % 10 != 0) ? " " : "") +
                    convertNumber((int) (number % 10));
        }
        if (countDigitsInNumber == 3) {
            femaleNumber = false;
            return Hundreds.getWordForNumber((int) (number / 100)) + ((number % 100 != 0) ? " " : "") +
                    convertNumber(number % 100);
        }
        if (countDigitsInNumber > 3 && countDigitsInNumber < 7) {
            femaleNumber = true;
            int divider = (int) Math.pow(10, 3);
            thousandForm = Thousands.getThousandProperForm((int) (number / divider));
            return convertBigNumber(number, divider, thousandForm);
        }
        if (countDigitsInNumber >= 7) {
            int power = (countDigitsInNumber % 3 == 0) ?
                    countDigitsInNumber - 3 : countDigitsInNumber - (countDigitsInNumber % 3);
            int divider = (int) Math.pow(10, power);
            BigNumbers bigNumber = BigNumbers.getNumber(divider);
            bigNumberForm = BigNumbersForm.getNumberProperForm((int) (number / divider), bigNumber);
            return convertBigNumber(number, divider, bigNumberForm);
        } else return numberInWords;
    }

    /*
        Method for converting big numbers which have the same structure (million, billion , etc)
     */
    private static String convertBigNumber(long number, long digit, String numberProperForm) {
        return convertNumber(number / digit) + " " + numberProperForm + ((number % digit != 0) ? " " : "")
                + convertNumber(number % digit);
    }
}
