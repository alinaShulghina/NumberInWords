package com.softserve.edu.alina_shulghina;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alin- on 17.12.2017.
 */
public class NumberParserTest {

    @Before
    public void resetFemaleNumber() {
        NumberParser.femaleNumber = false;
    }

    @Test
    public void convertNumberTest1() {
        assertEquals("один", NumberParser.convertNumber(1));
    }

    @Test
    public void convertNumberTest16() {
        assertEquals("шестнадцать", NumberParser.convertNumber(16));
    }

    @Test
    public void convertNumberTest20() {
        assertEquals("двадцать", NumberParser.convertNumber(20));
    }

    @Test
    public void convertNumberTest100() {
        assertEquals("сто", NumberParser.convertNumber(100));
    }

    @Test
    public void convertNumberTest150() {
        assertEquals("сто пятьдесят", NumberParser.convertNumber(150));
    }

    @Test
    public void convertNumberTest111111111() {
        assertEquals("сто одиннадцать миллионов сто одиннадцать тысяч сто одиннадцать",
                NumberParser.convertNumber(111111111));
    }

    @Test
    public void convertNumberTest111111111111() {
        assertEquals("сто одиннадцать миллиардов сто одиннадцать миллионов сто одиннадцать тысяч" +
                        " сто одиннадцать",
                NumberParser.convertNumber(111111111111L));
    }

    @Test
    public void convertNumberTest12123() {
        assertEquals("двенадцать тысяч сто двадцать три", NumberParser.convertNumber(12123));
    }

    @Test
    public void convertNumberTest3000(){
        assertEquals("три тысячи",NumberParser.convertNumber(3000));
    }

    @Test
    public void convertNumberTest11111(){
        assertEquals("одиннадцать тысяч сто одиннадцать",NumberParser.convertNumber(11111));
    }

    @Test
    public void convertNumberTest14567124(){
        assertEquals("четырнадцать миллионов пятьсот шестьдесят семь тысяч сто двадцать четыре",
                NumberParser.convertNumber(14567124));
    }

    @Test
    public void convertNumberTest14000(){
        assertEquals("четырнадцать тысяч",NumberParser.convertNumber(14000));
    }

    @Test
    public void convertNumberTest15311763981(){
        assertEquals("пятнадцать миллиардов триста одиннадцать миллионов" +
                        " семьсот шестьдесят три тысячи девятьсот восемьдесят один",
                NumberParser.convertNumber(15311763981L));
    }

    @Test
    public  void convertNumberTest1000(){
        assertEquals("одна тысяча",NumberParser.convertNumber(1000));
    }

    @Test
    public void convertNumberTest1000000000(){
        assertEquals("один миллиард",NumberParser.convertNumber(1000000000));
    }

    @Test
    public void convertNumberTest14951(){
        assertEquals("четырнадцать тысяч девятьсот пятьдесят один",NumberParser.convertNumber(14951));
    }
}